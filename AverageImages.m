%% Averaging pics
        % Credit: Lindsay Santacroce (contact: lindsayas22@gmail.com), 2022
        % Description: This function takes as many images as you'd like and averages the pixels to create a composite of the images. This can be used in research to check image similarity (colors, subject locations, etc.) between conditions, or for a personalized abstract art piece that commemorates a person, place, or event.
        % Instructions:
            % 1. Place all of the images you want to average in a single folder
            % 2. Name the folder something descriptive (e.g., "Exp1Targets"), which will later become part of the composite image's name
            % 3. Copy that folder's path
            % 4. Enter "AverageImages('[the folder's path]')" into the MATLAB command window and hit enter (add script's folder to path if you haven't already done so)
            % 5. The code will create a new subfolder called "CompositeImages", create the composite image, save to the subfolder, and display the image
            % 6. You can change the images you have in the original image folder, repeat steps 1-4, and a second(+) composite image will appear in the same subfolder without overwriting the first

function AverageImages(imfolder)
cd(imfolder);

%% Naming things based on current folder/project
catPos = strfind(imfolder, '\'); %get positions of '\'s in folder
catName = imfolder(catPos(end)+1:end); %make the category name everything after the last '/' (the folder's name)
imCatName = [catName, '_Composite']; %name minus iteration #

%getting number of averaged images already made or making a new folder for the averages if they haven't been done yet (so they can mess around with their pics without overwriting averaged images)
imageList = dir(imfolder);
coName = 'CompositeImages';
coFolderName = [imfolder, '\', coName, '\'];
if ismember(coName, {imageList(:).name})
    averages = dir(coFolderName);
    for av = numel(averages):-1:1
        if contains(averages(av).name, '.png') == 0
            averages(av) = [];
        end
    end
    coIts = numel(averages)+1;
else %if an average hasn't been made, make new folder and make this the first iteration
    mkdir(coFolderName);
    coIts = 1;
end
coImName = [coFolderName, imCatName, num2str(coIts), '.png']; %final image file name

%% Doing the thing
%all images
for i = numel(imageList):-1:1
    try %if it's an image
        imfinfo(imageList(i).name);
    catch
        imageList(i) = [];
    end
end

%start with the first image
p = 1;
image = double(imread(imageList(p).name));

%read images 2 - N and add to first image
for p = 2:numel(imageList)
    tempImage = imread(imageList(p).name);
    image = image+double(tempImage);
end

%divide summed pixel values by number of images
image = uint8(image./numel(imageList));
imwrite(image, coImName);
imshow(image);

end
