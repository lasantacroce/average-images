# Average Images (MATLAB function)
Credit: Lindsay Santacroce (contact: lindsayas22@gmail.com)

## Description
This function takes as many images as you'd like and averages the pixels to create a composite of the images. This can be used in research to check image similarity (colors, subject locations, etc.) between conditions, or for a personalized abstract art piece that commemorates a person, place, or event.

## Instructions
1. Download the .m file and add its location to your MATLAB path

2. Place all of the images you want to average in a single folder

3. Name the folder something descriptive (e.g., "Exp1Targets"), which will later become part of the composite image's name

4. Copy that folder's path

5. Enter "AverageImages('[the folder's path]')" into the MATLAB command window and hit enter (add script's folder to path if you haven't already done so)

6. The code will create a new subfolder called "CompositeImages", create the composite image, save to the subfolder, and display the image

7. You can change the images you have in the original image folder, repeat steps 2-5, and a second(+) composite image will appear in the same subfolder without overwriting the first

## Visuals
**Experiment 4 Object Images Composite:**

![](Example%20Gifs/Composite_E4Images_gif.gif)       ![](Example%20Gifs/ABvEAB_Experiment4Images_Average3.png)



**Graduation Pics Composite:**

![](Example%20Gifs/Composite_Graduation_gif.gif)       ![](Example%20Gifs/GraduationAverage1.png)

## Support
With any questions/comments/ideas for additions, contact me at lindsayas22@gmail.com and include "Average Images" in the subject line.
